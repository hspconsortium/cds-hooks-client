package org.hspconsortium.cdshooks.service;

import org.hspconsortium.cdshooks.model.ServiceInvocation;
import org.hspconsortium.cdshooks.model.ServiceResponse;

@FunctionalInterface
public interface ExecutableService {

    ServiceResponse execute(ServiceInvocation serviceInvocation);

}
