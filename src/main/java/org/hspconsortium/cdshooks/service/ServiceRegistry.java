package org.hspconsortium.cdshooks.service;

import org.hspconsortium.cdshooks.model.ServiceDefinition;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class ServiceRegistry {

    private Map<String, ExecutableService> registry = new HashMap<>();

    public ServiceRegistry add(ExecutableService service) {
        registry.put(((ServiceDefinition) service).getId(), service);
        return this;
    }

    public Collection<ExecutableService> getServices() {
        return registry.values();
    }

    public ExecutableService get(String serviceId) {
        return registry.get(serviceId);
    }

}
