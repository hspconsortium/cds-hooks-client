package org.hspconsortium.cdshooks.converter;

import ca.uhn.fhir.context.FhirContext;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.springframework.stereotype.Component;

@Component
public class StringToResourceConverter {

    private static final FhirContext FHIR_CONTEXT = FhirContext.forDstu3();

    public <T extends IBaseResource> T toResource(String resourceString, Class<T> type) {
        return type.cast(FHIR_CONTEXT.newJsonParser().parseResource(resourceString));
    }

}
