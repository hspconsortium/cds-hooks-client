package org.hspconsortium.cdshooks.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceResponse {

    private List<Card> cards = null;

    private List<Decision> decisions = null;

    public ServiceResponse addCard(Card card) {
        Validate.isTrue(decisions == null, "Cannot add card to response after decisions have been added.");
        if (cards == null) {
            cards = new ArrayList<>();
        }
        cards.add(card);
        return this;
    }

    public List<Card> getCards() {
        return cards == null ? null : Collections.unmodifiableList(cards);
    }

    public ServiceResponse addDecision(Decision decision) {
        Validate.isTrue(cards == null, "Cannot add card to response after decisions have been added.");
        if (decisions == null) {
            decisions = new ArrayList<>();
        }
        decisions.add(decision);
        return this;
    }

    public List<Decision> getDecisions() {
        return decisions == null ? null : Collections.unmodifiableList(decisions);
    }

}