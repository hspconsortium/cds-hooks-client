package org.hspconsortium.cdshooks.model;

import ca.uhn.fhir.model.api.IResource;

import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;

public class Suggestion {

    private String label;

    private UUID uuid;

    private Collection<IResource> create;

    private Collection<String> delete;

    public String getLabel() {
        return label;
    }

    public Suggestion setLabel(String label) {
        this.label = label;
        return this;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Suggestion setUuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }

    public Suggestion addCreate(IResource resource) {
        if (create == null) {
            create = new LinkedList<>();
        }
        create.add(resource);
        return this;
    }

    public Suggestion addDelete(String deleteId) {
        if (delete == null) {
            delete = new LinkedList<>();
        }
        delete.add(deleteId);
        return this;
    }
}
